include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml

variables:
  DEFAULT_IMAGE: "registry.gitlab.com/coderdojo-kinsale/gitlab-ci-containers/maven:3-amazoncorretto-11"
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task

.default_rules:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"  # Add the job to merge request pipelines if there's an open merge request.
    - if: $CI_OPEN_MERGE_REQUESTS  # Don't add it to a *branch* pipeline if it's already in a merge request pipeline.
      when: never
    - if: $CI_COMMIT_BRANCH # If there's no open merge request, add it to a *branch* pipeline instead.

stages:
  - .pre
  - build
  - test
  - visualise
  - image
  - release
  - deploy
  - .post

secret_detection:
  stage: .pre
  rules:    
    - !reference [.default_rules, rules]

build:
  services:
  - name: docker:dind
    command: ["--tls=false"]
  variables:
    DOCKER_HOST: "tcp://docker:2375"
    DOCKER_TLS_CERTDIR: ""
    DOCKER_DRIVER: overlay2
    MAVEN_OPTS: >-
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
    MAVEN_CLI_OPTS: >-
      --batch-mode
      --errors
      --fail-at-end
      --show-version
      --no-transfer-progress
  stage: build
  image: $DEFAULT_IMAGE
  script:
  - mvn $MAVEN_CLI_OPTS package -Dskiptests=true
  artifacts:
    when: always
    paths:
    - target/*.jar
  rules:    
    - !reference [.default_rules, rules]

tests:
  services:
  - name: docker:dind
    command: ["--tls=false"]
  variables:
    DOCKER_HOST: "tcp://docker:2375"
    DOCKER_TLS_CERTDIR: ""
    DOCKER_DRIVER: overlay2
    MAVEN_OPTS: >-
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
    MAVEN_CLI_OPTS: >-
      --batch-mode
      --errors
      --fail-at-end
      --show-version
      --no-transfer-progress
  stage: test
  image: $DEFAULT_IMAGE
  script:
  - mvn $MAVEN_CLI_OPTS verify
  - awk -F"," '{ covered += $7 + $9; total += $6 + $7 + $8 + $9 } END { print "Coverage     -",
    100*covered/total, "%" }' target/site/jacoco/jacoco.csv
  artifacts:
    when: always
    paths:
    - target/site/jacoco/jacoco.xml
    - target/checkstyle-result.xml
    reports:
      junit:
      - target/surefire-reports/TEST-*.xml
      - target/failsafe-reports/TEST-*.xml
  coverage: "/Coverage     - ([0-9]{1,2}\\.{0,1}[0-9]{0,2})[0-9]{0,3}/"
  rules:    
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"  # Add the job to merge request pipelines if there's an open merge request.
      when: never
    - if: $CI_OPEN_MERGE_REQUESTS  # Don't add it to a *branch* pipeline if it's already in a merge request pipeline.
      when: never
    - if: $CI_COMMIT_BRANCH # If there's no open merge request, add it to a *branch* pipeline instead.

verify:
  services:
  - name: docker:dind
    command: ["--tls=false"]
  variables:
    DOCKER_HOST: "tcp://docker:2375"
    DOCKER_TLS_CERTDIR: ""
    DOCKER_DRIVER: overlay2
    MAVEN_OPTS: >-
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
  stage: visualise
  image: registry.gitlab.com/anthonygnolan/maven-gitlab:3-amazoncorretto-21
  cache:
    key: "${CI_JOB_NAME}"
    paths:
    - .sonar/cache
  script:
    - mvn verify sonar:sonar -Dsonar.organization=${CI_PROJECT_ROOT_NAMESPACE} -Dsonar.projectKey=${CI_PROJECT_ROOT_NAMESPACE}_${CI_PROJECT_NAME} -Dsonar.qualitygate.wait=true
    - awk -F"," '{ covered += $7 + $9; total += $6 + $7 + $8 + $9 } END { print "Coverage     -", 100*covered/total, "%" }' target/site/jacoco/jacoco.csv
    - python3 /opt/cover2cover.py target/site/jacoco/jacoco.xml $CI_PROJECT_DIR/src/main/java/ > target/site/cobertura.xml
    - "curl 'https://sonarcloud.io/api/issues/search?componentKeys='${CI_PROJECT_ROOT_NAMESPACE}_${CI_PROJECT_NAME}'&pullRequest='${CI_MERGE_REQUEST_IID} --header 'Authorization: Bearer '${SONAR_TOKEN_NEW} -v -o sonar.json"
    - python3 /opt/sonar2codeclimate.py
  coverage: "/Coverage     - ([0-9]{1,2}\\.{0,1}[0-9]{0,2})[0-9]{0,3}/"
  artifacts:
    reports:
      codequality:
        - gl-code-quality-report.json
      coverage_report:
        coverage_format: cobertura
        path: target/site/cobertura.xml
      junit:
        - target/surefire-reports/TEST-*.xml
        - target/failsafe-reports/TEST-*.xml
  rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      - when: never

sonarcloud-check:
  image: $DEFAULT_IMAGE
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar -Dsonar.organization=${CI_PROJECT_ROOT_NAMESPACE} -Dsonar.projectKey=${CI_PROJECT_ROOT_NAMESPACE}_${CI_PROJECT_NAME} -Dsonar.qualitygate.wait=true
  rules:    
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"  # Add the job to merge request pipelines if there's an open merge request.
      when: never
    - if: $CI_OPEN_MERGE_REQUESTS  # Don't add it to a *branch* pipeline if it's already in a merge request pipeline.
      when: never
    - if: $CI_COMMIT_BRANCH # If there's no open merge request, add it to a *branch* pipeline instead.

build-image:
  image: docker:20.10.16
  stage: image
  services:
    - docker:20.10.16-dind
  dependencies:
    - build
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:latest
    - docker tag $CI_REGISTRY_IMAGE:latest $IMAGE_TAG
    - docker push $IMAGE_TAG
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - when: never

container_scanning:
  stage: image
  needs: ["build-image"]
  variables:
    CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE:latest 
    CS_IMAGE: $CI_REGISTRY_IMAGE
  allow_failure: false
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - when: never

deploy_development:
  stage: deploy
  script:
    - echo "Deploy to development server"
  environment:
    name: development
    url: https://development.example.com
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - when: never

deploy_staging:
  stage: deploy
  script:
    - echo "Deploy to staging server"
  environment:
    name: staging
    url: https://staging.example.com
  rules:
    - if: $CI_COMMIT_TAG
    - when: never

deploy_production:
  stage: deploy
  script:
    - echo "Deploy to production server"
  environment:
    name: production
    url: https://production.example.com
  rules:
    - if: $CI_COMMIT_TAG
      when: manual
    - when: never 

pages:  
  stage: deploy  
  image: $DEFAULT_IMAGE
  needs: ["deploy_production"]
  script:
  - mvn site
  - mv target/site public
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_TAG
    - when: never